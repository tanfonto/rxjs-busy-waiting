import { Subject, timer, interval, race } from 'rxjs';
import { mapTo, map, filter, take } from 'rxjs/operators';

const abort$ = new Subject();
const empty = null;
const timeout = 500;
const tick = 10;

const query = () => document.getElementById('renders-late');

const waitFor = race(
  timer(timeout).pipe(mapTo(empty)),
  abort$.pipe(mapTo(empty)),
  interval(tick).pipe(
    map(() => query()),
    filter(x => x !== empty),
    take(1)
  )
);

setTimeout(() => {
  const p = document.createElement('p');
  p.id = 'renders-late';
  document.body.appendChild(p);
}, 250);

waitFor.subscribe(x => console.log(x));
